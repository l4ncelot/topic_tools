//
// Created by l4ncelot on 1/19/16.
//

#include <ros/ros.h>
#include <topic_tools/MuxSelect.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "mux_client");

    ros::NodeHandle nh("~");


    const char *service_name = "/mux/select";
    ros::ServiceClient client = nh.serviceClient<topic_tools::MuxSelect>(service_name);

    topic_tools::MuxSelect srv;

    ros::Rate r(1);
    std::vector<std::string> topic_names{"/Plane1/pcloud", "/Plane2/pcloud"};
    std::vector<std::string> fixed_frames{"Plane1/laser0_frame", "Plane2/laser0_frame"};
    unsigned int i = 0;
    int value = 1;
    while (ros::ok()) {
        srv.request.topic = topic_names.at(i);
        ros::param::set("/octomap_server/base_frame_id", fixed_frames.at(i));
        if (!client.call(srv)) {
            ROS_INFO("failed to call service %s", service_name);
        }
        i += value;
        value *= -1;
        r.sleep();
        ros::spinOnce();
    }

    return 0;
}

